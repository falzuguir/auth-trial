package com.ijsberen;

import com.ijsberen.domain.domainobject.User;
import com.ijsberen.domain.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Collections;

@SpringBootApplication
public class SpringOauth2Application implements CommandLineRunner {

    private final UserRepository userRepository;

    @Lazy
    private final PasswordEncoder passwordEncoder;

    public SpringOauth2Application(PasswordEncoder passwordEncoder, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringOauth2Application.class, args);
    }

    @Override
    public void run(String... args) {
        if (userRepository.findAll().size() == 0) {
            User user = new User("admin", "admin", passwordEncoder.encode("felippe"), Collections.singletonList("ADMIN"));
            User user2 = new User("user", "user", passwordEncoder.encode("felippe"), Collections.singletonList("USER"));

            this.userRepository.save(user);
            this.userRepository.save(user2);
        }
    }

}

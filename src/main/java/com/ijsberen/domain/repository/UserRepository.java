package com.ijsberen.domain.repository;

import com.ijsberen.domain.domainobject.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findByUsername(String username);

}
